
  

  

  

  

# git-flow-template

  

  

  

  

# Sumário

1.  [Introdução](#markdown-header-1-introducao)

   

2.  [Modelo de fluxo Git](#markdown-header-2-modelo-de-fluxo-git)


3.  [Configurando o Git](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html)

4.  [Modelo de fluxo sugerido](#markdown-header-modelo-de-fluxo-sugerido)


# 1. Introdução
 

O intuito é expor uma abordagem de como trabalhar utilizando o fluxo git. Se você já trabalha com o git como principal ferramenta de controle de versão, já deve ter visto várias abordagens de como utilizar e controlar branchs em um cenário de produção ou pessoal.

Este fluxo é apenas uma sugestão ele não é obrigatório, se você quiser fazer o fluxo da sua forma ou usar algum plug-in fique a vontade. Aqui vou expor a forma como eu faço. Sugestões e críticas serão bem-vindas.

E se você é novo com git, este fluxo irá te ajudar a ter maior familiaridade de como empresas e projetos opensource costumam utilizar seus fluxos de trabalho.
 

É muito comum vermos pessoas utilizando somente uma branch para fazer commits em projetos pessoais. Isto não é errado, é muito tranquilo de se controlar tudo em uma branch quando se está desenvolvendo sozinho, mas o cenário muda bastante quando temos que interagir com mais contribuidores, seja em um projeto opensource ou privado.


Links sobre o assunto:



[A successful git branching model](https://nvie.com/posts/a-successful-git-branching-model/)
 

[Introduction Git Flow](https://datasift.github.io/gitflow/IntroducingGitFlow.html)

[Utilizando-o-fluxo-git-flow](https://medium.com/trainingcenter/utilizando-o-fluxo-git-flow-e63d5e0d5e04)
 

Links de plug-ins para fazer git flow:
 

[git-flow-cheatsheet](https://danielkummer.github.io/git-flow-cheatsheet/index.pt_BR.html)

  

[gitflow](https://github.com/nvie/gitflow)
 

Links de programas para se usar o git:
 

[gitkraken](https://www.gitkraken.com/)
 

# 2. Modelo de fluxo Git

  

![Git Flow](https://lh3.googleusercontent.com/cVOltqAvThsHRqBgx_JiznUxc2-Zy3sx8iU312S_V9XajfWB__dWCHFJLB86iQ3-1BVvDos9jQRpug  "Git Flow")



# Modelo de fluxo sugerido
 

## Tipos de branch



As branches podem ser do tipo:
 

-  **feature**: Para novas funcionalidades.

  

  

  

  

  

-  **fix**: Correção de bugs em desenvolvimento.

  

  

  

  

  

-  **hotfix**: Correção de bugs em produção.

  

  

  

  

  

-  **docs**: Para alterações na documentação.

  

  

  

  

  

-  **style**: Para alterações que não afetam o código (espaço em branco, formatação, ponto e vírgula, etc).

  

  

  

  

  

-  **refactor**: Utilizado para refactor de código.

  

  

  

  

  

-  **performance**: Para alterações de melhoria de performance.

  

  

  

  

  

-  **test**: Adicionar testes que faltaram.

  

  

  

  

-  **chore**: Mudanças no processo de construção ou ferramentas auxiliares e bibliotecas para gerar documentação.

  

  

  

  

  

Todas as branches devem sair da **development** e voltar para **development**, exceto quando é uma branch do tipo **hotfix** que deve sair da **master** e voltar para **master** e **development**

  

  

  

  

## Passo 1 - Clonar

Se você ainda não tem o repostitório clonado você precisa [clonar repositório](#markdown-header-clonar-repositorio).

Se você já tem o repositório clonado você precisa apenas [atualizar repositório](#markdown-header-atualizar-repositorio) ele sempre que for começar a trabalhar.
  

## Clonar repositório



Abra o repositório e clique em clone:

  

  

  

![Clone - Step 1](https://lh3.googleusercontent.com/LqZGd8bsbuKVeJUoueE6JNbbM5DoNFy21v47xwdh6e8wJUlSGw8Kuze1ie6mmFzDbx5WEUkjcrOjSA  "Git Clone - Step 1")

  

  

  

Copie o link:

  

  

  

![Clone - Step 2](https://lh3.googleusercontent.com/jJXZ6hOjmr_3jH9fuj4YmqB-1W5bAQITatlt7XFsFR587u1veX0yw2NEA7jo9wTNMQVpsdxOuj8O6Q  "Git Clone - Step 2")

  

  

  

Abra o seu terminal na pasta desejada e execute o comando copiado

  

  

>git clone git@bitbucket.org:vileladiego/git-flow-template.git

  

  

  

## Atualizar repositório
  

Dar o comando **git fetch --all** e em seguida o **git rebase** para atualizar todas as branches do repositório na sua máquina. É muito importante que você mantenha suas branches atualizadas para evitar problemas com merge.

Atualizar todos repositórios locais

> git fetch --all



Fazer rebase com a development

> git rebase origin/development

  

## Passo 2 - Criar branch

  

Sempre que for começar a fazer algo [consulte aqui](#markdown-header-tipos-de-branch) qual é o tipo de branch a ser criada e crie para começar a trabalhar.

  

![enter image description here](https://lh3.googleusercontent.com/8FB-s61UQ-NJLOqv82NpKhxYZAwAt939ko4zWNpKmRzbtxxtxBuuXBBgFQKOBrsCvJaOpilsNwUMCA  "Criar branch")

  

  

atualiza todas as branches remote-tracking com o servidor com o comando abaixo:

  

> git fetch --all

  

  

Considerando que o id do item no Jira seja feature/18. Este commando cria a branch local "feature/18", a partir da branch "origin/develop", e realiza o seu checkout.

  

>git checkout -b feature/18 origin/develop

  

O comando abaixo cria a branch "feature/18" no remote "origin" (servidor) e seta ela como default para futuros pushes.

  

>git push --set-upstream origin feature/18

## Passo 3 - Commits
Este commando adiciona todos as modificações que serão enviadas para o repositório local.
> git add .

Envia os arquivos modificados para o repositório local
> git commit -m "[#18] - Mensagem do commit"

Envia os arquivos modificados para a branch "feature/18" do remote "origin" (servidor).
 >git push origin feature/18

## Passo 4 - Finalização
A finalização é realizada mediante rebase interativo, o qual irá reescrever todo o histórico de commits, juntando em apenas um. Executar os comandos abaixo:

>git fetch --all


No rebase, deixar apenas o primeiro commit como "pick". Os demais, substituir o "pick" por "s" (squash). Em seguida, salvar o arquivo e sair.

>git rebase -i origin/development
  

![enter image description here](https://lh3.googleusercontent.com/B9kooyI-T6C8v8ffEvaeWQWwoE5DAr1whZmPd3_G6aGwB44X-CjylhSWySvEs0jDPM4pmn62n6CCEQ)

Envia os arquivos modificados para a branch "feature/18" do remote "origin" (servidor). Utilizamos a opção "-f" para forçar o push após o rebase, **evitando um novo merge** entre a branch "origin/feature/18" e "feature/18".

>git push -f origin feature/18

## Passo 5 - Criar pull request

Agora é só abrir o repositório pelo seu navegador preferido, ir no menu lateral e escolher a opção de pull request.

![enter image description here](https://lh3.googleusercontent.com/9iciwwzCAz8y_I0gLYL2KbaLa27XfsrcHyDemcYrqQVdChu__OO7dC5rV4TIGhStRw2iLlMBEZgZTw "Pull Request - Step 1")

Clique em create a pull request

![enter image description here](https://lh3.googleusercontent.com/maHfz-2u3aKffaU0UwWVNkjy0MLk97oJTdge-bB2BxKX7tL3e5wlM9bToxv8gO-I7AIGxTwXLuHe_Q "Pull Request - Step 2")

Escolha a branch de origem (que no nosso caso é a feature/18) e a de destino (que sempre vai ser a development, com exceção apenas para hotfix). Coloque título, descrição e escolha o reviewer (que é quem vai revisar, aprovar ou reprovar) e clique em "Create pull request". Agora é só aguardar para ver se suas alterações serão aprovadas ou reprovadas.

![enter image description here](https://lh3.googleusercontent.com/q3QMH73bK9SRbl3E0Kq1SozgCG_82QNAMRlBf37Wl_G7qP-jjaRFT7Nf_ZRhdmfdD-F1iqEvLmm8Lw "Pull Request - Step 3")

## Revisar e rejeitar o pull request
Caso o pull request seja aprovado é só clicar em "Merge" e pronto. Caso contrário é preciso clicar em "Decline".

![enter image description here](https://lh3.googleusercontent.com/yLpDP5Wu7iQKBX4yxtipn14OAMQAUcNexfqg-P2L2mZ9hFVd3rLYJgLhZnBrIQVXF7LvnukP1_hbgg)

Após clicar em "Decline" será apresentada uma tela de confirmação e você pode escrever o motivo.
Depois de colocar o motivo você deve clicar em "Decline" novamente para confirmar.

![enter image description here](https://lh3.googleusercontent.com/lLuhj23Jli5Xx7gpfBzpUKpV9WC2tbdgFpQl4PNut10nRRI3LyO-PsKlR3jS_216kGfkvIFha9DdRg)

Agora o merge já está reprovado e não tem como voltar atrás. Você pode colocar comentários para que o desenvolvedor saiba o que deve arrumar para o que o merge seja aprovado. Nâo se esqueça de salvar cada comentário.

![enter image description here](https://lh3.googleusercontent.com/UhDB_Gc-pja1DrtommDVJ4VvTXj8tML7Fi736cZwqMTBruLns-Q5zCW3fopUcUl1G0GTaLcFWpgJTQ)

## Executar alterações

Depois de executar as alteração é hora de partir para o Pull Request novamente para que suas alterações irem para homologação. 

Primeiro adicione todas as alterações
> git add .

Depois faça o commit adicionando o :1 para indicar que é uma alteração
> git commit - "[#18:1]

Dê o push para o repositório remoto
> git push origin 

Agora é hora de juntar suas alterações com o que está na branch de development para que sua branch fique atualizada e não perca o código do amiguinho. Segue a lista de commandos antes de pedir um novo Pull Request.

Atualizar todos os repositórios locais
> git fetch --all

Fazer rebase com a branch remota
> git rebase -i origin feature/18

Neste rebase, **deixar os dois como "pick"**

![enter image description here](https://lh3.googleusercontent.com/DZmcBXoK9lkSu-6WpnpmNpMssWRqy3NLxanLbDSsf04adCtFEaxJC6Al6cOsrkH8K1RgsdJ3hUDkNg)

Agora vamos dar o push que vai para o servidor. Utilizamos a opção "-f" para forçar o push após o rebase, evitando um novo merge entre a branch "origin/feature/18" e "feature/18".
> git push -f origin feature/18

Envie novamente o "Pull Request" e aguarde a aprovação novamente.

# Importante 

Realize commits **diariamente** não deixe para commitar tudo na ultima hora. Assim você evita merges muito grandes.

Matenha sua branch **sempre** atualizada com a development, fazendo rebase.
> git rebase origin/develop

Executar testes unitários **sempre** que for feito um rebase.
